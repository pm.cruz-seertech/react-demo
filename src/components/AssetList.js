import React, { Component } from 'react';
import {Table, Button, Column} from 're-bulma';

class AssetList extends Component {
   constructor() {
    super();
        this.state = {
            data: [],
        }
    }

    componentDidMount() {
        fetch('http://127.0.0.1:5000/assets')
        .then(res => {
            if (!res.ok) return Promise.reject(new Error(`HTTP Error ${res.status}`));
                return res.json(); // parse json body
        })
        .then(data => {
        console.log(data);
                this.setState({
                    data: data
                })
        })
        .catch(err => console.error(err));
    }

    render() {
    const assetList = this.state.data.map((dynamicData) => {
        console.log("DATA", dynamicData)
        return (
            <tr>
                <td> {dynamicData.assetName}</td>
                <td> {dynamicData.assetType} </td>
                <td> {dynamicData.model} </td>
                <td> {dynamicData.datePurchased} </td>
                <td>
                    <Column hasTextAlign='centered'>
                        <Button isColor='success' isOutlined>Request</Button>
                    </Column>
                </td>
            </tr>
        )
    })
    return(
    <Table isBordered isStriped isNarrow>
        <thead>
            <h1>Asset List</h1>
            <tr>
                <th>Asset Name</th>
                <th>Asset Type</th>
                <th>Model</th>
                <th>Date Purchased</th>
                <th>Action</th>
            </tr>
        </thead>
        {assetList}
     </Table>
    )
    }

}

export default AssetList;