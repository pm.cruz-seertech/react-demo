import React, { Component } from 'react';
import { Box, Button, Column, Input, Label, Section} from 're-bulma';

class App extends Component {
  toLogin(){
    window.location.href = "/login";
  }
  render() {
    return (
      <Section>
        <h1>Create an Account</h1>
        <Column>
        <Box>
          <Label>Username:</Label>
          <Input
            color="isSuccess"
            type="text"
            placeholder="Username..."
            defaultValue=""
            icon="fa fa-check"
            hasIconRight
          />
          <Label>First Name:</Label>
          <Input
            color="isSuccess"
            type="text"
            placeholder="First Name..."
            defaultValue=""
            icon="fa fa-check"
            hasIconRight
          />
          <Label>Last Name:</Label>
          <Input
            color="isSuccess"
            type="text"
            placeholder="Last Name..."
            defaultValue=""
            icon="fa fa-check"
            hasIconRight
          />
          <Label>Password</Label>
          <Input
            color="isSuccess"
            type="password"
            placeholder="Password..."
            defaultValue=""
            icon="fa fa-check"
            hasIconRight
          />
          <Button
            isColor='primary'>Submit
          </Button>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <Button isColor='primary' onClick={this.toLogin}>Go To Login
          </Button>
        </Box>
      </Column>
      </Section>
    );
  }
}

export default App;
