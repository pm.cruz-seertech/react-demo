import React, { Component } from 'react';
import { Button, Column, Section} from 're-bulma';

class Home extends Component {
  onLogin(){
    window.location.href = "/register";
  }
  render() {
    return (
      <Section>
        <h1>Asset Management System</h1>
        <Column>
        <Button isColor='primary' onClick={this.onLogin}>Login
        </Button>
      </Column>
      </Section>
    );
  }
}

export default Home;