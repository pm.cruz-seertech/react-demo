import React, { Component } from 'react';
import {Box, Button, Column, Input, Label, Section} from 're-bulma';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
        user:'',
        pass:''
    }
  }
  toRegister(){
    window.location.href = "/register";
  }

  postLogin({event}){
    console.log("Login", this.state.user+'|'+this.state.pass)
    const logincredentials = {username: this.state.user, password: this.state.pass};
    fetch('http://127.0.0.1:5000/login', {
    method: 'post',
    body: JSON.stringify(logincredentials),
    headers: {
    'content-type': 'application/json',
    'accept': 'application/json'
    }
    })
    .then(res => {
            if (!res.ok) return Promise.reject(new Error(`HTTP Error ${res.status}`));
                return res.json(); // parse json body
        })
     .then(data => {
        let status = data[0].status
        let message = data[0].message
        console.log(status);
        console.log(message);
        if (status === 'success')
            window.location.href = "/assetList";
        else
            alert(message)
     })
    .catch(err => console.error(err));
  }

  render() {
    return (
      <Section>
        <h1>Login an Account</h1>
        <Column>
        <Box>
          <Label>Username:</Label>
          <Input
            color="isSuccess"
            type="text"
            placeholder="Username..."
            defaultValue=""
            icon="fa fa-check"
            hasIconRight
            onChange={(event)=>this.setState({user:event.target.value})}
          />
          <Label>Password</Label>
          <Input
            color="isSuccess"
            type="password"
            placeholder="Password..."
            defaultValue=""
            icon="fa fa-check"
            hasIconRight
            onChange={(event)=>this.setState({pass:event.target.value})}
          />
          <Button isColor='primary' onClick={(event) => this.postLogin(event)}>Submit
        </Button>
        &nbsp;&nbsp;&nbsp;&nbsp;
          <Button isColor='primary' onClick={this.toRegister}>Go Back
          </Button>
        </Box>
      </Column>
      </Section>
    );
  }
}

export default App;

