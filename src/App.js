import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Home from './components/Home';
import Login from './components/Login';
import Register from './components/Register';
import AssetList from './components/AssetList';
import ApproveDenyAsset from './components/ApproveDenyAsset';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={Home}/>
          <Route path="/login" component={Login}/>
          <Route path="/register" component={Register}/>
          <Route path="/assetlist" component={AssetList}/>
          <Route path="/approvedenyasset" component={ApproveDenyAsset}/>
        </div>
      </Router>
    );
  }
}

export default App;